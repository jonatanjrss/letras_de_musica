"""
"""


import tkinter as tk

import tkinter.ttk as ttk

from vagalume import lyrics

import os

import sys


class Gui(object):
    def __init__(self, master=None):
        self.master = master
        
        fr1 = ttk.Frame(master)
        fr1.pack()

        fr2 = ttk.Frame(master)
        fr2.pack()

        fr3 = ttk.Frame(master)
        fr3.pack()
    
        ttk.Label(
            fr1, text='BUSCADOR DE LETRAS DE MÚSICAS').pack(
                pady=40)

        ttk.Label(
            fr2, text='ARTISTA').grid(row=0, column=0)

        ttk.Label(
            fr2, text='MÚSICA').grid(row=1, column=0)

        self.et1 = ttk.Entry(fr2, width=25)
        self.et1.grid(row=0, column=1, pady=20, padx=6)
        self.et2 = ttk.Entry(fr2, width=25)
        self.et2.grid(row=1, column=1)

        ttk.Button(fr3, text='LETRA',
                   command=self.search).grid(row=0, column=0,
                                             pady=20, padx=5)
        ttk.Button(fr3, text='TRADUÇÃO',
                   command=self.tradution).grid(row=0, column=1,
                                               padx=5)

        ttk.Button(fr3, text='SAIR',
                   command=self.close).grid(row=0, column=2,
                                            padx=5)


    def search(self):
        self.find_music(self.et1.get(), self.et2.get())

    def tradution(self):
        result = lyrics.find(self.et1.get(),
                             self.et2.get())
        
        filename = self.et2.get()+'.txt'

        f = open('musica.txt', 'w')

        translation = result.get_translation_to('pt-br')
        
        f.write(translation.lyric)
        
        f.close()
        
        cmd = 'leafpad "{}"'.format('musica.txt')
        os.system(cmd)
        sys.exit()

        
    def close(self):
        self.master.destroy()
    
    def find_music(self, artista, musica):
        res = lyrics.find(artista, musica)

        filename = musica+'.txt'
        f = open('musica.txt', 'w')
        f.write(res.song.lyric)
        f.close()

        cmd = 'leafpad "{}"'.format('musica.txt')
        os.system(cmd)
        sys.exit()
    
       
root = tk.Tk()
root.geometry('400x400')
Gui(root)
root.mainloop()
